package com.jspider.maven_basic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jspider.maven_basic.entity.Company;
import com.jspider.maven_basic.entity.Country;
import com.jspider.maven_basic.entity.Department;
import com.jspider.maven_basic.entity.Movie;
import com.jspider.maven_basic.entity.Payment;
import com.jspider.maven_basic.entity.PrimeMinister;
import com.jspider.maven_basic.repository.AssociationImpl;
import com.jspider.maven_basic.repository.MovieRepository;
import com.jspider.maven_basic.repository.PaymentRepository;

public class App 
{
    public static void main( String[] args )
    {
       Movie movie = new Movie();

       movie.setName("wttt");
       movie.setBudget(100D);
       movie.setRating("4");
       movie.setReleaseDate(new Date());
       
       
 //---------------------------------------------------------------------------------     
//       Payment pay = new Payment();
//       pay.setId(1L);
//       pay.setMovieId(123L);
//       pay.setNumberOfTicket(5L);
//       pay.setPrice(100D);
//       pay.set
//       pay.setShowTime(showTime);
//       pay.setTotalPrice(totalPrice);
       
       
 
       
//--------------------------------------------------------------------------------
//       MovieRepository movieRepository = new MovieRepository();
//       
//       PaymentRepository paymentRepository = new PaymentRepository();
       
       
      // movieRepository.saveMovieDetails(movie);  
       
//       paymentRepository.saveMovieDetails(pay);
       
//-----------------------------------------------------------------------------------
       Country country = new Country();
       
       country.setArea("3.28 million squre km");
       country.setContinent("Asia");
       country.setName("India");
       country.setNumberOfState(32L); 
       country.setPopulation("150000000");
       
        
       PrimeMinister primeMinister = new PrimeMinister();
       
       primeMinister.setAge(65L);
       primeMinister.setGender("Male");
       primeMinister.setPmName("Mr.Modi");
       primeMinister.setPoliticalParty("BJP");
       primeMinister.setQualification("UG");
       
       country.setPrimeMinister(primeMinister);
       
       AssociationImpl associationImpl = new AssociationImpl();
       //associationImpl.saveCountryDetails(country);
//-----------------------------------------------------------------------------
       Department dept = new Department();
       
       dept.setDepartmentName("Sales");
       dept.setManagerName("Vishal");
       dept.setNoOfEmployee(250L);
       	
       Department dept1 = new Department();
       dept1.setDepartmentName("Account");
       dept1.setManagerName("Sillon");
       dept1.setNoOfEmployee(100L);
       
       Department dept2 = new Department();
       dept2.setDepartmentName("Developer");
       dept2.setManagerName("Satyajeet");
       dept2.setNoOfEmployee(300L);
       
       
       List <Department> deptList = new ArrayList<Department>();
       
       deptList.add(dept);
       deptList.add(dept1);
       deptList.add(dept2);
       
       
       Company cmp = new Company();
       
       cmp.setName("ABC PVT.LTD.");
       cmp.setTotalEmployee(1000L);
       cmp.setHeadQuaters("Baharagora");
       cmp.setCeoName("Mitun");
       cmp.setDepartmentName(deptList);
       
       associationImpl.saveCompanyDetails(cmp);
       
       
       
    }
}
 