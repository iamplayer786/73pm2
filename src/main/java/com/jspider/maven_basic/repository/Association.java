package com.jspider.maven_basic.repository;

import com.jspider.maven_basic.entity.Company;
import com.jspider.maven_basic.entity.Country;

public interface Association {
	public void saveCountryDetails(Country country);
	public void saveCompanyDetails(Company company);

}
