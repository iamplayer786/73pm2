package com.jspider.maven_basic.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.jspider.maven_basic.entity.Company;
import com.jspider.maven_basic.entity.Country;

public class AssociationImpl implements Association{

	@Override
	public void saveCountryDetails(Country country) {
		Configuration cfg = new Configuration();
		cfg.configure();
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(country);
		transaction.commit();
		
	}

	@Override
	public void saveCompanyDetails(Company company) {
		Configuration cfg = new Configuration();
		cfg.configure();
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(company);
		transaction.commit();  
		
	}
	
	

}
