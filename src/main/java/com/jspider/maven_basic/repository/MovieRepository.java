
package com.jspider.maven_basic.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.jspider.maven_basic.entity.Movie;


public class MovieRepository {
	public void saveMovieDetails(Movie movie)
	{
		try
		{
			Configuration cfg = new Configuration();
			cfg.configure();
			SessionFactory sessionFactory = cfg.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(movie);
			transaction.commit();
		}
			
		catch(HibernateException e)
		{
			
		}
	}
			
}
	

	
