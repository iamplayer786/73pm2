package com.jspider.maven_basic.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.jspider.maven_basic.entity.Movie;
import com.jspider.maven_basic.entity.Payment;
import com.jspider.maven_basic.utility.HibernateUtility;

public class PaymentRepository {
	public void saveMovieDetails(Payment pay)
	{
		try
		{
			Session session = HibernateUtility.getConnection().openSession();
			Transaction transaction = session.beginTransaction();
			session.save(pay);
			transaction.commit();
		}
			
		catch(HibernateException e)
		{
			
		}
	}

}
