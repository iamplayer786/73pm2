package com.jspider.maven_basic.constant;

public interface AppConstants {
	public static String MOVIE_INFO = "movie_info";
	public static String PAYMENT_INFO = "payment_info";
	public static String COMPANY_INFO = "company_info";
	public static String DEPARTMENT_INFO = "department_info";

}
