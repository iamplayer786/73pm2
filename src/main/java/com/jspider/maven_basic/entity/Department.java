package com.jspider.maven_basic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspider.maven_basic.constant.AppConstants;

@Entity
@Table(name =AppConstants.DEPARTMENT_INFO)
public class Department implements Serializable{
	@Id
	@GenericGenerator(name="c_auto",strategy="increment")
	@GeneratedValue(generator="c_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="department_Name")
	private String departmentName;
	
	@Column(name="manager_name")
	private String managerName;
	
	@Column(name="no_of_employee")
	private Long noOfEmployee;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public Long getNoOfEmployee() {
		return noOfEmployee;
	}
	public void setNoOfEmployee(Long noOfEmployee) {
		this.noOfEmployee = noOfEmployee;
	}
	
	
	

}
