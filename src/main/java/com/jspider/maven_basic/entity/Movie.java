package com.jspider.maven_basic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspider.maven_basic.constant.AppConstants;

@Entity
@Table(name = AppConstants.MOVIE_INFO)
public class Movie implements Serializable  
{ 
	@Id
	@GenericGenerator(name = "m_auto", strategy = "increment")
	@GeneratedValue(generator= "m_auto")
	
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="rating")
	private String rating;
	
	@Column(name="budget")
	private Double budget;
	
	@Column(name="releaseDate")
	private Date releaseDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public Double getBudget() {
		return budget;
	}
	public void setBudget(Double budget) {
		this.budget = budget;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	
	
	
}
