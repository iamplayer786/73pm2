package com.jspider.maven_basic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspider.maven_basic.constant.AppConstants;


@Entity
@Table(name=AppConstants.COMPANY_INFO)
public class Company implements Serializable{
	
	@Id
	@GenericGenerator(name="c_auto",strategy = "increment")
	@GeneratedValue(generator="c_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "company_name")
	private String name;
	
	@Column(name = "ceo_name")
	private String ceoName;
	
	@Column(name = "headQuaters")
	private String headQuaters;
	
	@Column(name = "totalEmployee") 
	private Long totalEmployee;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="id")
	private List<Department> departmentList;
	
	
	public List<Department> getDepartmentName() {
		return departmentList;
	}
	public void setDepartmentName(List<Department> departmentName) {
		this.departmentList = departmentName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCeoName() {
		return ceoName;
	}
	public void setCeoName(String ceoName) {
		this.ceoName = ceoName;
	}
	public String getHeadQuaters() {
		return headQuaters;
	}
	public void setHeadQuaters(String headQuaters) {
		this.headQuaters = headQuaters;
	}
	public Long getTotalEmployee() {
		return totalEmployee;
	}
	public void setTotalEmployee(Long totalEmployee) {
		this.totalEmployee = totalEmployee;
	}
	
	


}
