package com.jspider.maven_basic.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.GenericGenerators;

@Entity
@Table
public class Country implements Serializable{
	@Id
	@GenericGenerator(name = "m_auto", strategy = "increment")
	@GeneratedValue(generator= "m_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "county_name")
	private String name;
	
	@Column(name = "numberofState")
	private Long numberOfState;

	@Column(name = "area")
	private String area;
	
	@Column(name = "continent")
	private String continent;
	
	@Column(name = "population")
	private String population;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "for_key")
	private PrimeMinister primeMinister;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public PrimeMinister getPrimeMinister() {
		return primeMinister;
	}
	public void setPrimeMinister(PrimeMinister primeMinister) {
		this.primeMinister = primeMinister;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getNumberOfState() {
		return numberOfState;
	}
	public void setNumberOfState(Long numberOfState) {
		this.numberOfState = numberOfState;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getContinent() {
		return continent;
	}
	public void setContinent(String continent) {
		this.continent = continent;
	}
	public String getPopulation() {
		return population;
	}
	public void setPopulation(String population) {
		this.population = population;
	}
	
	public Country() {
		
	}
	
	

}
