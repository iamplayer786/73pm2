package com.jspider.maven_basic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table()
public class PrimeMinister implements Serializable{
	
	@Id
	@GenericGenerator(name = "m_auto", strategy = "increment")
	@GeneratedValue(generator= "m_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "primeMinister_Name")
	private String pmName;
	
	@Column(name = "age")
	private Long age;
	
	@Column(name = "gender")
	private String gender;

	@Column(name = "political_Party")
	private String politicalParty;
	
	@Column(name = "qualification")
	private String qualification;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPmName() {
		return pmName;
	}

	public void setPmName(String pmName) {
		this.pmName = pmName;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPoliticalParty() {
		return politicalParty;
	}

	public void setPoliticalParty(String politicalParty) {
		this.politicalParty = politicalParty;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	
	public PrimeMinister() {
		
	}
	
	
	

}
