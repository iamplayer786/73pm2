package com.jspider.maven_basic.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspider.maven_basic.constant.AppConstants;


	
@Entity
@Table(name=AppConstants.PAYMENT_INFO)
public class Payment implements Serializable{
	@Id
	@GenericGenerator(name = "m_auto", strategy = "increment")
	@GeneratedValue(generator= "m_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="movieId")
	private Long movieId;
	
	@Column(name="numberOfTicket")
	private Long numberOfTicket;
	
	@Column(name="price")
	private Double price;
	
	@Column(name="showDate")
	private Date showDate;
	
	@Column(name="showTime")
	private String showTime;
	
	@Column(name="paymentType")
	private String paymentType;
	
	@Column(name="totalPrice")
	private Double totalPrice;
	public Long getId() {
			return id;
	}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getMovieId() {
			return movieId;
		}
		public void setMovieId(Long movieId) {
			this.movieId = movieId;
		}
		public Long getNumberOfTicket() {
			return numberOfTicket;
		}
		public void setNumberOfTicket(Long numberOfTicket) {
			this.numberOfTicket = numberOfTicket;
		}
		public Double getPrice() {
			return price;
		}
		public void setPrice(Double price) {
			this.price = price;
		}
		public Date getShowDate() {
			return showDate;
		}
		public void setShowDate(Date showDate) {
			this.showDate = showDate;
		}
		public String getShowTime() {
			return showTime;
		}
		public void setShowTime(String showTime) {
			this.showTime = showTime;
		}
		public String getPaymetType() {
			return paymentType;
		}
		public void setPaymetType(String paymetType) {
			this.paymentType = paymetType;
		}
		public Double getTotalPrice() {
			return totalPrice;
		}
		public void setTotalPrice(Double totalPrice) {
			this.totalPrice = totalPrice;
		}
		
		public Payment(){}
		
		

}
