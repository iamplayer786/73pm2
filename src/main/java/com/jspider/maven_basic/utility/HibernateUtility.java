package com.jspider.maven_basic.utility;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtility {
	
	private static SessionFactory so = null;
	public HibernateUtility() {
		
	}
	public static SessionFactory getConnection()
	{
		if(so==null)
		{

			so = new Configuration().configure().buildSessionFactory();
		}
		return so;

	}
}
